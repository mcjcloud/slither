var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port =3001;
app.use(express.static(__dirname + '/public'));

http.listen(port, () => console.log('listening'));