var timeLimit = 3000.0;
var timePassed = 0.0;

// produce n food at random locations
function populateFood(n, game, food, width, height) {
    for(var i = 0; i < n; i++) {
        var x = Math.random() * width;
        var y = Math.random() * height;
        food.push(game.add.sprite(x, y, 'food'));
    }
}

function updateFood(game, food, width, height) {
    timePassed += game.time.elapsed;
    if(timePassed >= timeLimit) {
        // create a piece of food
        var x = Math.random() * width;
        var y = Math.random() * height;
        food.push(game.add.sprite(x, y, 'food'));
        
        // reset time
        timePassed = 0.0;
    }
}

function eat(game, snake, food, score) {
    var sBounds = snake[0].getBounds();
    for(var i = food.length - 1; i >= 0; i--) {
        var p = food[i];
        //console.log("P: " + p);
        var pBounds = p.getBounds();
        if(Phaser.Rectangle.intersects(sBounds, pBounds)) {
            // eat the food
            food.splice(i, 1);
            p.destroy();

            console.log("score: " + score);
            // grow the snake for every 10 points
            if((score + 1) % 10 == 0 && score > 0) {
                grow(game, snake);
            }

            score += 1;
        }
    }

    return score;
}
