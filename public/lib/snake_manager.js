function startPhysics(game, snake) {
    game.physics.startSystem(Phaser.Physics.P2JS);
    for(var i = 0; i < snake.length; i++) {
        game.physics.enable(snake[i], Phaser.Physics.ARCADE);
    }
}

function updateMovement(game, snake, speed) {

    // move the player toward the cursor at a givenspeed
    for(var i = 0; i < snake.length; i++) {
        // if its the head, move toward pointer
        if(i == 0) {
            game.physics.arcade.moveToPointer(snake[0], speed);
        }
        else {  // else move the snake part toward the part before it
            //console.log("current: " + snake[i].body);
            //console.log("last: " + snake[i - 1].body);
            game.physics.arcade.moveToObject(snake[i], snake[i - 1], speed);
        }
    }

    //  if it's overlapping the mouse, don't move any more
    // get the offset from the screen left and right
    var xOffset = game.camera.x;
    var yOffset = game.camera.y;
    if (Phaser.Rectangle.contains(snake[0].body, game.input.x + xOffset, game.input.y + yOffset)) {
        for(var i = 0; i < snake.length; i++) {
            snake[i].body.velocity.setTo(0, 0);
        }
    }
}

function grow(game, snake) {
    // get vector from player to cursor and set next location in that direction
    var xDir = sign((game.input.x + game.camera.x) - snake[0].body.x);
    var yDir = sign((game.input.y + game.camera.y) - snake[0].body.y);
    console.log("dirs " + xDir + " " + yDir);

    snake.push(game.add.sprite(snake[snake.length - 1].body.x - (10 * xDir), snake[snake.length - 1].body.y - (10 * yDir), 'circle'));
    game.physics.enable(snake[snake.length - 1], Phaser.Physics.ARCADE);
    //game.physics.arcade.moveToObject(snake[snake.length - 1], snake[snake.length - 2], speed);
    // print the coords of each part
    for(var i = 0; i < snake.length; i++) {
        console.log("i: " + i + "(" + snake[i].body.x + ", " + snake[i].body.y + ")");
    }
}

// return 1 or -1 based on the sign of a number
function sign(x) {
    return (x >= 0) ? 1 : -1;
}
