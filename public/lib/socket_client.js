var enemySnakes = [];
var enemyIds = [];

var socket;

function initSocket(game) {
    console.log('init socket called');
    socket = io.connect('http://localhost:3030');
    // if(!socket.connected) {
    //     socket = io.connect('http://10.0.1.80:3030');
    // }


    socket.on('connect', function(data) {
        console.log('connectclient');
        socket.emit('handshake', 'handshake_data');
    });

    socket.on('init', function(data) {
        console.log("got id");
        id = data.id;
        console.log('clients: ' + JSON.stringify(data));
        for(var i = 0; i < data.clients.length; i++) {
            enemySnakes.push([game.add.sprite(game.world.centerX, game.world.centerY, 'circle')]);
            enemyIds.push(data.clients[i]);
        }
        console.log('enemies: ' + enemySnakes.length);
    });

    socket.on('new', function(data) {
        console.log('new snake');
        // create a new snake
        var snake = [game.add.sprite(game.world.centerX, game.world.centerY, 'circle')];
        enemySnakes.push(snake);
        enemyIds.push(data.id);
    });

    socket.on('update', function(data) {
        console.log('update client');
        var enemies = data.enemies;
        for(var enemy in enemies) {
            var enemyIndex = enemyIds.indexOf(enemy.id);
            if(enemyIndex >= 0) {
                var snake = enemySnakes[enemyIndex];
                // update the snake pieces
                for(var i = 0; i < snake.length; i++) {
                    snake[i].x = enemy.pieces[i].x;
                    snake[i].y = enemy.pieces[i].y;
                }
                // add new snake pieces
                for(var i = snake.length; i < enemy.pieces.length; i++) {
                    snake.push(game.add.sprite(enemy.pieces[i].x, enemy.pieces[i].y, 'circle'));
                }
            }
            else {
                console.log("NOT FOUND");
            }
        }
        console.log('emitting snake');
        socket.emit('update', snake);
        //syncServer(game)
    });
}

function syncServer(snake) {
    console.log('sync server');
    socket.emit('update', snake);
}