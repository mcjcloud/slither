var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.CANVAS, 'test', {
	preload: preload,
	create: create,
	update:update,
	render:render
});

var width = 10000;
var height = 10000;

var id =0;
var snake = new Array(0);
var score = 0;

var food = new Array();
var speed =200;

function preload() {
	game.load.image('circle', 'lib/res/sonic_swirl.png');
	game.load.image('background','lib/res/greyswords.jpg');
	game.load.image('food','lib/res/jerry.png');

	//initSocket(game);
}


function create() {
	game.add.tileSprite(0,0,width,height, 'background');
	game.world.setBounds(0, 0, width, height);

	// add food
	populateFood(1000,game,food,game.world.width,game.world.height);
	
	snake.push(game.add.sprite(game.world.centerX, game.world.centerY, 'circle'));
	
	startPhysics(game, snake); 
	game.camera.follow(snake[0]);	
}

function update() {
	updateMovement(game, snake, speed);
	updateFood(game,food,game.world.width,game.world.height);
	score=eat(game, snake, food, score);
}
function render() {
	
}
















